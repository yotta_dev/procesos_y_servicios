package c_ejercicio_2_Compilador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JTree;
import javax.swing.border.LineBorder;
import java.awt.Color;
/**
 * 
 * @author Yotta_Dev
 *
 */
@SuppressWarnings("serial")
public class CompiladorClasesJava extends JFrame {

	private JPanel contentPane;
	private JButton btnCompilar;
	private File classACompilar;
	private JTree arbolDirectorio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompiladorClasesJava frame = new CompiladorClasesJava();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CompiladorClasesJava() {
		setTitle("Compilador JAVA by Yotta_Dev");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 299, 312);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnSeleccionarFicherojava = new JButton("Seleccionar Fichero .java");
		btnSeleccionarFicherojava.setBounds(30, 25, 238, 25);
		btnSeleccionarFicherojava.setFont(new Font("Dialog", Font.BOLD, 10));
		btnSeleccionarFicherojava.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				classACompilar=seleccionarFichero();
				
			}
		});
		contentPane.setLayout(null);
		contentPane.add(btnSeleccionarFicherojava);
		
		btnCompilar = new JButton("Compilar (.java) a (.class)");
		btnCompilar.setBounds(30, 62, 238, 25);
		btnCompilar.setFont(new Font("Dialog", Font.BOLD, 10));
		btnCompilar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					controlDeEjecucion(classACompilar);
					mostrarInfoDirectorio(classACompilar);
					
				} catch (IOException e1) {
					
					mostrarMensaje(e1.getMessage());
					
				}
			}
		});
		btnCompilar.setEnabled(false);
		contentPane.add(btnCompilar);
		
		arbolDirectorio = new JTree();
		arbolDirectorio.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("\tArchivos") {
				{
				}
			}
		));
		arbolDirectorio.setEnabled(false);
		arbolDirectorio.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		arbolDirectorio.setBounds(30, 99, 238, 156);
		contentPane.add(arbolDirectorio);
	}
	
	private File seleccionarFichero() {
		
		JFileChooser selectorDeFicheros = new JFileChooser();

		selectorDeFicheros.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		FileNameExtensionFilter filtroImagen=new FileNameExtensionFilter(".JAVA","java");
		
	    selectorDeFicheros.setFileFilter(filtroImagen);

		int op = selectorDeFicheros.showOpenDialog(this);
		
		File directorioAListar = null;

		if (op == JFileChooser.APPROVE_OPTION) {

			directorioAListar = selectorDeFicheros.getSelectedFile(); 

		}
		
		btnCompilar.setEnabled(true);
		arbolDirectorio.setEnabled(true);
		
		mostrarInfoDirectorio(directorioAListar);
		
		return directorioAListar;
	}
	
	private void mostrarInfoDirectorio(File directorio) {
		
		if (directorio!=null) {
			
			File directorioPadre = new File(directorio.getParent());
			File[] listaArchivos = directorioPadre.listFiles();
			
			DefaultMutableTreeNode principal = new DefaultMutableTreeNode(directorioPadre.getName());
			DefaultTreeModel modelo;
			DefaultMutableTreeNode nodo;
			
			for (File listaArchivo : listaArchivos) {
			     //Creamos un nodo con el nombre del archivo en el que estamos y se lo agregamos al nodo raíz
			     nodo = new DefaultMutableTreeNode(listaArchivo.getName());
			     principal.add(nodo);
			}
			
			modelo = new DefaultTreeModel(principal, true);
			arbolDirectorio.setModel(modelo);
		}
		
		
	}

	private void controlDeEjecucion(File ficheroSeleccionado) throws IOException {
		
		ProcessBuilder builder = new ProcessBuilder().command("javac", ficheroSeleccionado.getAbsolutePath());

		Process process = builder.start();
		
		process.getOutputStream();
		
		mostrarMensaje("Se ha compilado el .JAVA a .Class Correctamente");
		
		btnCompilar.setEnabled(false);
	}
	
	private void mostrarMensaje(String mensaje) {
		
		JOptionPane.showMessageDialog(null, mensaje, "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
		
	}
}
