package h_syncronized_ejemplo_2;

public class Taquilla {

	public static void main(String[] args) {

		Teatro teatro = new Teatro();

		Espectador uno = new Espectador(1, teatro, (int) (Math.random() * (30 - 45 + 1) + 45));
		Espectador dos = new Espectador(2, teatro, (int) (Math.random() * (30 - 45 + 1) + 45));

		uno.start();
		dos.start();

		try {

			uno.join();
			dos.join();

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Los clientes de Madrid han comprado " + 
							teatro.entradasCompradas_PorEspectadores_DeMadrid + " entradas.");
		
		System.out.println("Los clientes de fuera de Madrid han comprado " + 
							teatro.entradasCompradas_PorEspectadores_OtrasProvincias + " entradas.");
		
		System.out.println("Quedan por vender en taquilla " + teatro.localidadesTeatro + " entradas.");

	}

}
