package h_syncronized_ejemplo_2;

import java.util.Random;

public class Espectador extends Thread {

	// ATRIBUTOS
	boolean es_De_Madrid;
	int idComprador;
	Teatro teatro;
	int entradasAComprar;

	// CONSTRUCTOR
	public Espectador(int idComprador, Teatro t, int entradasAComprar) {
		super();
		this.idComprador = idComprador;
		this.teatro = t;
		this.entradasAComprar = entradasAComprar;

		// Calculamos el boolean:
		Random random = new Random();
		es_De_Madrid = random.nextBoolean(); // Aleatorio si es de madrid o no
	}

	public void run() {
		while (true) {
			synchronized (teatro) {
				if (teatro.localidadesTeatro >= entradasAComprar) {
					teatro.comprarEntradas(es_De_Madrid, entradasAComprar);
				} else {
					break;
				}

				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} // Fin sync
		} // fin while
	}

}
