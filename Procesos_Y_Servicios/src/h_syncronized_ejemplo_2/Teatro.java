package h_syncronized_ejemplo_2;

public class Teatro {
	// ATRIBUTOS
	public int localidadesTeatro = 1240;
	public int entradasCompradas_PorEspectadores_DeMadrid = 0;
	public int entradasCompradas_PorEspectadores_OtrasProvincias = 0;

	// CONSTRUCTOR
	public Teatro() {
		super();
	}

	public void comprarEntradas(boolean es_De_Madrid, int entradasAComprar) {

		//COMPROBACION DE NUMERO DE ENTRADAS DISPONIBLES
		if (localidadesTeatro >= entradasAComprar) {

			localidadesTeatro -= entradasAComprar;

			if (es_De_Madrid) {

				entradasCompradas_PorEspectadores_DeMadrid += entradasAComprar;

			} else {

				entradasCompradas_PorEspectadores_OtrasProvincias += entradasAComprar;

			}
		}
	}
}
