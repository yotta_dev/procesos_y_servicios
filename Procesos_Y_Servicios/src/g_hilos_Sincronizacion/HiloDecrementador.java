package g_hilos_Sincronizacion;

public class HiloDecrementador extends Thread {

	Turno turno = new Turno();

	public HiloDecrementador(Turno turno) {
		super();
		this.turno = turno;
	}

	@Override
	public void run() {
		synchronized (turno) {
			
			for (int i = 100; i > 0; i--) {

				while (turno.getturno() != false) {
					try {
						turno.wait();
					} catch (InterruptedException e) {
	
						e.printStackTrace();
					}
				}

				System.out.println("---Hilo Decrementador: " + i);

				turno.setTurno(true);

				turno.notifyAll();

			}
		}
	}

}
