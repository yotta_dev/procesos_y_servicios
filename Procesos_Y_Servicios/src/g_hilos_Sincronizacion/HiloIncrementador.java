package g_hilos_Sincronizacion;

public class HiloIncrementador extends Thread {

	Turno turno = new Turno();

	public HiloIncrementador(Turno turno) {
		super();
		this.turno = turno;
	}

	@Override
	public void run() {

		synchronized (turno) {

			for (int i = 0; i < 100; i++) {

				while (turno.getturno() != true) {

					try {
						turno.wait();
					} catch (InterruptedException e) {
			
						e.printStackTrace();
					}

				}

				System.out.println("Hilo Incrementador: " + i);

				turno.setTurno(false);

				turno.notifyAll();

			}
		}

	}
}
