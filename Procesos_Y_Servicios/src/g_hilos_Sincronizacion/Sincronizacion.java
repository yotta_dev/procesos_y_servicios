package g_hilos_Sincronizacion;

public class Sincronizacion {

	public static void main(String[] args) throws InterruptedException {
		
		Turno turno = new Turno();
		
		HiloIncrementador hiloIncrementador = new HiloIncrementador(turno);
		
		HiloDecrementador hiloDecrementador = new HiloDecrementador(turno); 
		
		hiloDecrementador.start();

		hiloIncrementador.start();
		
	}

}
