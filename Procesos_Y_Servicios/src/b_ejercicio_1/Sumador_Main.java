package b_ejercicio_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 1. Crear una clase Java que sea capaz de sumar todos los números comprendidos
 * entre dos valores incluyendo ambos valores. Para resolverlo crearemos una
 * clase Sumador que tenga un método que acepte dos números n1 y n2 y que
 * devuelva la suma de todo el intervalo. Además, incluiremos un método main que
 * ejecute la operación de suma tomando los números de la línea de comandos (es
 * decir, se pasan como argumentos al main). Una vez hecha la prueba de la clase
 * sumador, le quitamos el main, y crearemos una clase que sea capaz de lanzar
 * varios procesos.
 * 
 * @author Yotta_Dev
 *
 */
public class Sumador_Main {

	public static void main(String[] args) {
		
		try {
		
			Process process = new ProcessBuilder(args).start();
			
			System.out.println("Proceso Iniciado: ");
			
			InputStream is = (InputStream) process.getInputStream();
			
			InputStreamReader isr = new InputStreamReader(is);
			
			BufferedReader br = new BufferedReader(isr);
			
			String line;
			
			while ((line = br.readLine()) != null) {
				
				System.out.println(line);
				
			}
		} catch (IOException e) {
			
			System.out.println(e.getMessage());
			
		}
		

	}

}
