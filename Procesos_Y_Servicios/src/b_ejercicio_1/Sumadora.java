package b_ejercicio_1;

public class Sumadora {

	public static void main(String[] args) {
		sumar(Integer.valueOf(args[0]),Integer.valueOf(args[1]));
	}
	
	public static void sumar(int n1,int n2) {
		
		int resultado = 0;

		if (n1 > n2) {
			
			for (int i = n2; i < n1; i++) {
				
				System.out.print(i+" + ");
				resultado+=i;
			}
			
			System.out.print(n1);
			
			resultado+=n1;		

		} else {
			
			for (int i = n1; i < n2; i++) {
				
				System.out.print(i+" + ");
				resultado+=i;
			}
			
			System.out.print(n2);
			
			resultado+=n2;
			
		}
		
		System.out.print(" = "+resultado);
		
	}
	
}
