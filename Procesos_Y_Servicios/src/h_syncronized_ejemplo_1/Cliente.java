package h_syncronized_ejemplo_1;

public class Cliente extends Thread {

	Cuenta cuenta;
	String nombre;
	double cantidad;
	double total = 0;

	Cliente() {
	}

	Cliente(double cantidad) {
		this.cantidad = cantidad;
	}

	public void run() {

		/*
		 * AQUÃ� HE MODIFICADO EL EJERCICIO PARA EVITAR QUE FALLE,
		 * POR MUCHAS EJECUCIONES QUE HAYA HECHO NO ENCONTRÃ‰ FALLO
		 * PERO POR LÃ“GICA EL PROGRAMA SIN SINCRONIZAR EL OBJETO PODRÃ�A
		 * FALLAR EN LA COMPROBACIÃ“N DE SALDO DISPONIBLE, LOS DOS HILOS
		 * PODRIAN ACCEDER A LA MISMA VARIABLE Y DAR PASO A LA RESTA,
		 * EL TEMA ESTÃ� EN QUE NO FALLA NUNCA YA QUE POR MUCHAS VUELTAS
		 * QUE HAGA EL BUCLE LA ÃšNICA OPORTUNIDAD DE FALLAR ES EN LA ÃšLTIMA
		 * ÃšNICAMENTE, POR ESO PARECE QUE NO FALLA, PERO PODRÃ�A FALLAR.
		 * 
		 * (EL FALLO SERÃ�A DAR SALDO NEGATIVO AL FINALIZAR
		 * 
		 * DE ESTA MANERA NO FALLARÃ�A
		 */
			while (true) {
				synchronized (cuenta) { //SINCRONIZO EL OBJETO
					if (cuenta.comprobar(cantidad)) {
						cuenta.restar(cantidad);
						total += cantidad;
					} else
						break;
				}

				try {
					Thread.sleep(1); //CEDO EL TESTIGO
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		

	}

}
