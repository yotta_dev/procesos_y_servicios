package h_syncronized_ejemplo_1;

public class Cuenta {

	double saldo;
	int numero_reintegros;
	
	public double verSaldo(){
		return this.saldo;
	}
	
	public void restar(double cantidad){
		this.saldo-=cantidad;
	}
	
	public boolean comprobar(double cantidad){
		if(this.saldo>=cantidad){
			return true;
		}else{
			return false;
		}
	}

}
