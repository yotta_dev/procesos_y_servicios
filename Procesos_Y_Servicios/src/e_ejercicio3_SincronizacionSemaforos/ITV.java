package e_ejercicio3_SincronizacionSemaforos;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Nos disponemos a simular cómo funcionaría una ITV mediante hilos. Habrá una
 * clase ITV con N puestos de inspección que tienen que atender a M clientes que
 * llevan su vehículo, de tal forma que: M será un valor aleatorio entre 20 y
 * 50, y N será un número entre 1 y 5. El tiempo que tarda un vehículo en ser
 * atendido será aleatorio entre 10msg y 100msg. Cuando un puesto termina de
 * atender a un coche, debe indicar por consola el mensaje: “El puesto x ha
 * atendido al vehículo y” ; por lo que, tanto los puestos como los coches
 * tienen que estar identificados por un número.Se debe crear un thread para
 * cada vehículo y otro para cada puesto de inspección. Los parámetros M y N se
 * deben calcular al principio del programa, mostrando su valor al comienzo de
 * este: “Hay M vehículos que serán atendidos en N puestos de inspección” Cuando
 * todos los vehículos terminen de ser revisados, se mostrará el mensaje: “Se
 * cierra la ITV”
 * 
 * @author Yehoshua
 *
 */
public class ITV {

	public static void main(String[] args) throws InterruptedException {
		//Generacion aleatoria de numero de clientes y puestos
		int numeroClientes=generarNumeroAleatorioDentroDeRango(20,50);
		int numeroPuestosInspeccion = generarNumeroAleatorioDentroDeRango(1,5);
		int tiempoDeEspera;
		
		ArrayList<Cliente>clientes= new ArrayList<>();
		ArrayList<Puesto_Inspeccion>puestos = new ArrayList<>();
		
		//Creacion de Vehiculos y asignacion de tiempo
		for (int i = 0; i < numeroClientes; i++) {
			
			tiempoDeEspera = generarNumeroAleatorioDentroDeRango(10, 100);
				
			Cliente vehiculo = new Cliente("Vehiculo "+i,tiempoDeEspera);
					
			clientes.add(vehiculo);
					
		} 
		
		//Creación de Puestos ITV
		for (int i = 0; i < numeroPuestosInspeccion; i++) {
			
			Puesto_Inspeccion puesto_Inspeccion = new Puesto_Inspeccion("Puesto "+i);
			
			puestos.add(puesto_Inspeccion);
		}
		
		
		
		System.out.println("Hay "+numeroClientes+" vehículos que serán atendidos en "+numeroPuestosInspeccion+" puestos de inspección.\n");
		
		Semaphore semaphore = new Semaphore(2);
		
		
		for (int i = 0; i < clientes.size(); i++) {
			
			
			semaphore.acquire();
			
			int puesto = generarNumeroAleatorioDentroDeRango(1, numeroPuestosInspeccion);
			
			puestos.get(puesto-1).run(clientes.get(i));
								
			semaphore.release();
			
		}
		
	}

	private static int generarNumeroAleatorioDentroDeRango(int min, int max) {

		int numero = ThreadLocalRandom.current().nextInt(min, max + 1);

		return numero;

	}

}
