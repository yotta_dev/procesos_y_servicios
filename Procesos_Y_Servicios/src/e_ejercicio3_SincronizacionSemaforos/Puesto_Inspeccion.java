package e_ejercicio3_SincronizacionSemaforos;

public class Puesto_Inspeccion extends Thread {
	String numero_Puesto;
	int tiempoDeEspera;

	public Puesto_Inspeccion(String numero_Puesto) {
		super();
		this.numero_Puesto = numero_Puesto;
	}

	public String getNumero_Puesto() {
		return numero_Puesto;
	}

	public void setNumero_Puesto(String numero_Puesto) {
		this.numero_Puesto = numero_Puesto;
	}

	public void run(Cliente cliente) {
		super.run();
		System.out.println("ITV: "+numero_Puesto+
							"\n--Cliente: "+cliente.nombreVehiculo+
							"\n--Tiempo Espera: "+cliente.tiempoDeEspera);  
        try {                           
             Thread.sleep(cliente.getTiempoDeEspera()*100);  
        } catch (InterruptedException e) {  
             e.printStackTrace();  
        }
        System.out.println("Terminando:"+cliente.nombreVehiculo+"\n");
	}
	
	
	
	
}
