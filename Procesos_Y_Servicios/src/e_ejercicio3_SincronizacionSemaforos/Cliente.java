package e_ejercicio3_SincronizacionSemaforos;

public class Cliente extends Thread {
	String nombreVehiculo;
	int tiempoDeEspera;

	public Cliente(String numeroVehiculo, int tiempoDeEspera) {
		super();
		this.nombreVehiculo = numeroVehiculo;
		this.tiempoDeEspera = tiempoDeEspera;
	}

	public String getNumeroVehiculo() {
		return nombreVehiculo;
	}

	public void setNumeroVehiculo(String numeroVehiculo) {
		this.nombreVehiculo = numeroVehiculo;
	}
	
	public int getTiempoDeEspera() {
		return tiempoDeEspera;
	}

	public void setTiempoDeEspera(int tiempoDeEspera) {
		this.tiempoDeEspera = tiempoDeEspera;
	}

	@Override
	public void run() {
		super.run();
		
        
	}
	
	
}
