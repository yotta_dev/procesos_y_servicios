package j_UD_2_3_Sockets_Ejercicio2;

import java.io.*;
import java.net.*;

class A_Servidor {
	static final int PUERTO = 5000;

	public static void main(String[] arg) {
		//SERVER
		try {
			
			ServerSocket serverSocket = new ServerSocket(PUERTO);

			System.out.println("SERVIDOR_LEVANTADO_Y_ESCUCHANDO");
			
			
			

			for (int numCli = 0; numCli < 3; numCli++) {

				Socket socket_Cliente = serverSocket.accept();

				System.out.println("Cliente :" + numCli + " conectado");

				//Creamos Flujo de salida del Servidor
				DataOutputStream dataOutputStream = new DataOutputStream(socket_Cliente.getOutputStream());
				
				//Alimentamos el flujo de Entrada del Servidor con el Input del Cliente
				DataInputStream dataInputStream = new DataInputStream(socket_Cliente.getInputStream());

				// Saludamos al Cliente segun se conecta por el Stream de Salida
				dataOutputStream.writeUTF("Hola cliente :" + numCli);

				// Una vez realizado lo anterior cerramos la conexion con el cliente
				socket_Cliente.close();
			}
			
			serverSocket.close();
			
			System.out.println("LIMITE DE CLIENTES ALCANZADO");

		} catch (Exception e) {

			System.out.println(e.getMessage()+"\n LIMITE DE CLIENTES ALCANZADO");

		}
	}

}