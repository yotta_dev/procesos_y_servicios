package j_UD_2_3_Sockets_Ejercicio2;

import java.io.*;
import java.net.*;

class B_Cliente {
	static final String IP = "localhost";
	static final int PUERTO = 5000;

	public static void main(String[] arg) {
		// CLIENTE
		try {
			// Creamos SocketCliente para realizar la conexion con el Servidor
			Socket socket = new Socket(IP, PUERTO);

			//Stream de Entrada de Datos mediante Teclado
			DataInputStream dataInputStream = new DataInputStream(System.in);
			
			//Stream de Salida de Datos hacia el Servidor
			DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
			
			
			
			System.out.println(dataInputStream.readUTF());

			// Por ultimo cerramos la conexión
			socket.close();

		} catch (Exception e) {
			
			System.out.println(e.getMessage()+" LIMITE DE CLIENTES ALCANZADO");
			
		}

	}
}
