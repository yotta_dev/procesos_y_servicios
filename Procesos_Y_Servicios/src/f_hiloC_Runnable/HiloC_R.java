package f_hiloC_Runnable;

/**
 * Repite el ejercicio 2 utilizando Runnable, cambiando las clases HiloC y
 * VariosHilos por HiloCR y VariosHilosR (R de Runnable)
 * 
 * @author Yehoshua
 *
 */
public class HiloC_R implements Runnable {

	int c;
	

	public HiloC_R(int c) {
		super();
		this.c = c;
		System.out.println("Creando Hilo "+c);
	}

	

	@Override
	public void run() {
		
		for (int i = 0; i < 5; i++) {
			
			System.out.println("Hilo: "+c+" linea: "+i);
			
			
		}
		
	}

}
