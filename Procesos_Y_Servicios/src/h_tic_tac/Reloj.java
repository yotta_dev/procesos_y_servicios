package h_tic_tac;
/**
 * 3. Crea dos nuevos hilos llamados HiloTic e HiloTac. 
 * 	  El primero debe visualizar en pantalla un bucle
 *    infinito con la palabra �TIC�, y el segundo con la palabra �TAC�.
 *    Crea la clase HiloReloj, que en su m�todo main cree e inicie los 
 *    hilos anteriores.Introduce despu�s de la sentecia de escritura de
 *    cada hilo un retardo de 1 segundo. Utiliza el m�todo sleep(long mils), 
 *    donde se indica el tiempo en milisegundos. Este m�todo arroja excepciones
 *    InterruptedException, por lo que debes a�adir un bloque try-cath para capturarla.
 *    
 * @author Yehoshua
 *
 */
public class Reloj {

	public static void main(String[] args) throws InterruptedException {
		//Instancio dos objectos Hilo TIC y TAC
		HiloTic tics=new HiloTic();
		HiloTac tacs=new HiloTac();
		
		//Inicio la ejecucion de tics, que en su interior tras ejecutar
		//la marca de TIC se espera 2000 milisegundos
		tics.start();
		
		//Esperar
		tics.sleep(1000);
		
		tacs.start();
	}
}