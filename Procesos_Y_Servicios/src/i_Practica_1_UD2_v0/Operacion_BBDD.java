package i_Practica_1_UD2_v0;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class Operacion_BBDD {

	Connection connection;
	int numeroRegistrosAInsertar;

	public Operacion_BBDD(int total_A_Insertar) {
		super();	
		this.numeroRegistrosAInsertar = total_A_Insertar;
		
		try {

			Class.forName("com.mysql.jdbc.Driver");

			String cadenaConexion = "jdbc:mysql://localhost:3306/BBDD_PSP_1";
			String usuario = "root";
			String password = "root";

			connection = (Connection) DriverManager.getConnection(cadenaConexion, usuario, password);

			System.out.println("Conectado");

		} catch (SQLException e) {

			System.out.println(e.getMessage().toString() + "SQL");

		} catch (ClassNotFoundException e) {

			System.out.println(e.getMessage().toString() + "Class");

		}
	}

	
	public void insertarRegistro(String id_Thread) {
		
		try {

			String EMAIL;
			int INGRESOS;

			PreparedStatement preparedStatement = null;

			EMAIL = id_Thread + "@gmail.com";

			INGRESOS = (int) Math.floor(Math.random() * (1000 - 10 + 1) + 10);

			String prep = "INSERT INTO T_INDIVIDUOS (ID,EMAIL,INGRESOS) VALUES (null,?,?)";

			preparedStatement = connection.prepareStatement(prep);

			preparedStatement.setString(1, EMAIL);
			preparedStatement.setInt(2, INGRESOS);

			preparedStatement.executeUpdate();

			preparedStatement.close();
			
			numeroRegistrosAInsertar--;

		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}

	}

}
