package i_Practica_1_UD2_v0;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class Hilo_Insert extends Thread {

	// ATRIBUTOS CONSTRUCTOR
	Operacion_BBDD operacion_BBDD;
	String id_Thread;

	// ATRIBUTOS LOCALES DE LA CLASE
	Connection connection;

	public Hilo_Insert(Operacion_BBDD operacion_BBDD, String id_Thread) {
		super();
		this.operacion_BBDD = operacion_BBDD;
		this.id_Thread = id_Thread;

	}

	@Override
	public void run() {

		synchronized (operacion_BBDD) {

			operacion_BBDD.insertarRegistro(id_Thread);

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}					

		}

	}

}
