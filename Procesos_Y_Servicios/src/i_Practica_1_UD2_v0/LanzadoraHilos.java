package i_Practica_1_UD2_v0;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class LanzadoraHilos {
	/**
	 * Aplicacion 1: A traves de consola, se solicita al usuario un numero de
	 * registros a insertar.
	 * 
	 * A traves de consola, solicita al usuario un numero de hilos.
	 * 
	 * Inserta el numero de registros introducido en una base de datos utilizando el
	 * numero de hilos indicado.
	 * 
	 * Los datos se generan de forma aleatoria, debiendo estar los ingresos
	 * comprendidos entre 10 y 1000
	 * 
	 * @param args
	 */

	public static void main(String[] args) {

		System.out.print("�Numero de Registros a Insertar? : ");
		int total_A_Insertar = Entrada.entero();

		System.out.print("�Numero de Hilos? : ");
		int numeroHilos = Entrada.entero();

		Operacion_BBDD operacion_BBDD = new Operacion_BBDD(total_A_Insertar);

		while (operacion_BBDD.numeroRegistrosAInsertar > 0) {

			for (int i = 0; i < numeroHilos; i++) {

				Hilo_Insert hilo_Insert = new Hilo_Insert(operacion_BBDD, "hilo_" + i);

				hilo_Insert.start();

				try {

					hilo_Insert.join();

				} catch (InterruptedException e) {

					e.getMessage();
				}

			}

		}

	}

}
