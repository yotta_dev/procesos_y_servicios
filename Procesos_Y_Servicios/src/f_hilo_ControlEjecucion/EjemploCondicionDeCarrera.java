package f_hilo_ControlEjecucion;

public class EjemploCondicionDeCarrera {
    public static void main(String[] args) {
        Hilo h1 = new Hilo(1,1);//Hilo sumador;
        Hilo h2 = new Hilo(2,-1);//Hilo restador
        Hilo h3 = new Hilo(3,1);//Hilo sumador;
        Hilo h4 = new Hilo(4,-1);//Hilo restador
        h1.start();
        h2.start();
        h3.start();
        h4.start();
    }   
}
