package j_UD_2_3_Sockets_Ejercicio1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class A_Servidor {

	public static void main(String[] args) {
		//Informacion de Red y Conexion al Server
		final int PUERTO_ECO = 5000;
		ServerSocket server_Socket = null;
		Socket socket_Cliente = null;
		//Streams Estandar de Comunicación
		PrintWriter printWriter;
		BufferedReader bufferedReader;

		try {
			
			int cnt = 0;

			server_Socket = new ServerSocket();

			InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost", PUERTO_ECO);

			server_Socket.bind(inetSocketAddress);

			while (cnt < 5) {
				
				System.out.println("- Esperando Conexión Cliente -");
				socket_Cliente = server_Socket.accept();

				cnt++;//Contabilizamos el cliente conectado
				
				//Stream de salida del Servidor
				printWriter = new PrintWriter(new OutputStreamWriter(socket_Cliente.getOutputStream()), true);
				
				//Stream de Entrada del Servidor
				bufferedReader = new BufferedReader(new InputStreamReader(socket_Cliente.getInputStream()));
				
				//PASO 1:El servidor envía un mensaje indicando el nº de clie
				if (socket_Cliente != null && printWriter != null) {
					System.out.println("------------------------------------------------------------");
					System.out.println("- Cliente Conectado nº: "+cnt);
					
					printWriter.println("- ¿Cual es tu nombre de Usuario?");
					
					//PASO 2:El servidor pide un nombre de usuario, que el usuario envía			
					String nombreCliente = bufferedReader.readLine();
					System.out.println("- Nombre de Usuario: "+nombreCliente);
					
					//PASO 3:El servidor envía una contraseña(nº  aleatorio  de  4  cifras).
					System.out.println("- Generando Contraseña ...");					
					//Random random = new Random();
					int password = (int)(10000 * Math.random());
					printWriter.println(String.valueOf(password));//Enviando la Contraseña al Cliente
					
					System.out.println("- Fin Conexion Cliente nº "+cnt+" "+nombreCliente);
					System.out.println("------------------------------------------------------------");
					
				}						
				socket_Cliente.close();
			}
			
			server_Socket.close();

		} catch (IOException e) {

			System.err.println("Fallo en la conexion: " + e);

		}
	}

}
