package j_UD_2_3_Sockets_Ejercicio1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class B_Cliente {

	public static void main(String[] args) {

		final int PUERTOECO = 5000;
		Socket socket_Cliente = null;
		
		BufferedReader bufferedReader_readServer;
		BufferedReader input_Teclado = new BufferedReader(new InputStreamReader(System.in));
		
		PrintWriter printWriter;

		try {
			socket_Cliente = new Socket();

			InetSocketAddress address = new InetSocketAddress("localhost", PUERTOECO);
			
			System.out.println("- Conectando con el Servidor -");
			socket_Cliente.connect(address);			
			System.out.println("- Conectado -");
			
			//Stream de Entrada del Cliente
			bufferedReader_readServer = new BufferedReader(new InputStreamReader(socket_Cliente.getInputStream()));
			
			//Stream de Salida del Cliente
			printWriter = new PrintWriter(new OutputStreamWriter(socket_Cliente.getOutputStream()), true);

			//Recibimos el numero de cliente asignado por el Servidor
			//y la solicitud de introducir un nombre del cliente
			String mensaje = bufferedReader_readServer.readLine();
			System.out.println("- Servidor: " + mensaje);
			
			//Introducimos el nombre del Cliente
			System.out.print("- Introduce un nombre de usuario : ");
			String nombreDeUsuario = input_Teclado.readLine();
			
			//Mandamos al Servidor el Nombre del Cliente
			printWriter.println(nombreDeUsuario);			
			
			//Recibimos la Contraseña generada por el Servidor
			String password = bufferedReader_readServer.readLine();
			System.out.println("- Servidor - Su Contraseña es: "+password);
			
			socket_Cliente.close();
			System.out.println("- Fin Conexión con el Servidor -");
			
			bufferedReader_readServer.close();

		} catch (UnknownHostException e) {
			System.err.println("Máquina desconocida: maquina");
		} catch (IOException e) {
			System.err.println("Fallo en la conexión: " + e);
		}

	}

}
