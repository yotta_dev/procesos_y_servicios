package j_UD_1_3_Sockets_Ejercicio3;

import java.io.*;
import java.net.*;

class Cliente {
	static final String IP = "localhost";
	static final int PUERTO = 5000;

	public static void main(String[] arg) {
		// CLIENTE
		try {
			// Creamos SocketCliente para realizar la conexion con el Servidor
			Socket socket = new Socket(IP, PUERTO);

			// Instaciamos ambos flujos de salida y entrada de Datos para que el cliente
			// Se pueda comunicar
			InputStream inputStream = socket.getInputStream();
			DataInputStream dataInputStream = new DataInputStream(inputStream);

			// Y mediante el flujo de salida estandar enviamos el Stream de salida del
			// cliente - Leemos con dataInput y mostramos con el Syso
			System.out.println(dataInputStream.readUTF());

			// Por ultimo cerramos la conexión
			socket.close();

		} catch (Exception e) {
			
			System.out.println(e.getMessage()+" LIMITE DE CLIENTES ALCANZADO");
			
		}

	}
}
