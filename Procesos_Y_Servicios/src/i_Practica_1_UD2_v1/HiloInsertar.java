package i_Practica_1_UD2_v1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;

public class HiloInsertar extends Thread{

	int nHilo;
	int nRegistros;
	
	int ingreso;
	
	Persistencia_BBDD bd;
	
	Connection cn=Persistencia_BBDD.conectarBBDD();
	
	public HiloInsertar(Persistencia_BBDD bd,int numeroRegistros, Connection con, int numeroHilo) {
		this.bd=bd;
		this.nRegistros=numeroRegistros;
		this.cn=con;
		this.nHilo=numeroHilo;
		
	}
	
	public void run() {
		
		while(bd.nRegistros>0) {
			
			//Los hilos que no estén actuando sobre el objeto bd, esperan.
			try {
				sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//Cuando puedan actuar sobre el objeto bd hacen todo el bloque.
			synchronized (bd) {
				if(bd.nRegistros>0) {
					Random r=new Random();
					
					//Creo la tabla si no existe
					Persistencia_BBDD.crearTabla(cn);
					
					ingreso=r.nextInt(990)+10;
					Persistencia_BBDD.insertarDatos(cn, "hilo"+nHilo+"@email.com", ingreso);	
					bd.nRegistros--;
				}
			}
			
		}
	}
	
}
