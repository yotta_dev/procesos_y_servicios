package i_Practica_1_UD2_v1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;

//CREATE TABLE T_INDIVIDUOS (ID INT PRIMARY KEY AUTO_INCREMENT, EMAIL VARCHAR(100),INGRESOS INT);

public class Main {

	public static void main(String[] args) {
		
		Connection cn=Persistencia_BBDD.conectarBBDD();
		
		//A través de consola, solicita al usuario un número de registros a insertar
		System.out.println("Número de registros a insertar:");
		int nRegistros = i_Practica_1_UD2_v0.Entrada.entero();
		
		//A través de consola, solicita al usuario un número de hilos
		System.out.println("Número de hilos a utilizar:");
		int nHilos = i_Practica_1_UD2_v0.Entrada.entero();
		
		/*
		Inserta el número de registros introducido en una base de datos utilizando el
		número de hilos indicado. Los datos se generarán de forma aleatoria, debiendo
		estar los ingresos comprendidos entre 10 y 1000.
		*/
		
		/*
		Genero el objeto bd para la sincronización y le indico una cantidad
		a la variable """controladora"""
		*/
		Persistencia_BBDD bd=new Persistencia_BBDD();
		bd.nRegistros=nRegistros;
		
		//Bucle que crea tantos hilos como hayan indicado
		for(int i=0;i<nHilos;i++) {
			Thread h = new Thread(new HiloInsertar(bd,bd.nRegistros, cn,i));
			h.start();
		}	
	}
}
