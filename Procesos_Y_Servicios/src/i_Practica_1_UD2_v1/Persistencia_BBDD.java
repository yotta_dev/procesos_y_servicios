package i_Practica_1_UD2_v1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Persistencia_BBDD {

	int nRegistros;
	
	public static Connection conectarBBDD(){
		
		Connection cn=null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
			System.err.println("No se ha encontrado el dirver de conexión.");
		}
		
		try {
			cn = DriverManager.getConnection("jdbc:mysql://localhost:3306/BBDD_PSP_1", "root", "root");
		} catch (SQLException ex) {
			System.err.println("Error en la conexión a la base de datos.");
		}
		
		return cn;
	}
	
	public static void crearTabla(Connection cn) {
		//CREO LA TABLA SI NO EXISTE
		PreparedStatement pstCrearTabla;
		String sqlCrearTabla = "CREATE TABLE IF NOT EXISTS T_INDIVIDUOS (id INTEGER AUTO_INCREMENT, email VARCHAR(100), ingresos INTEGER, PRIMARY KEY (id))";
				
		try {
			pstCrearTabla = cn.prepareStatement(sqlCrearTabla);
			pstCrearTabla.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void insertarDatos(Connection cn, String email, int ingreso) {
		PreparedStatement pstInsertarRegistro;
		String sqlSentencia = "INSERT INTO T_INDIVIDUOS(EMAIL,INGRESOS) VALUES(?,?)";
		
		try {
			pstInsertarRegistro = cn.prepareStatement(sqlSentencia);
			
			pstInsertarRegistro.setString(1,email);
			pstInsertarRegistro.setInt(2,ingreso);
			
			pstInsertarRegistro.executeUpdate();  
			
			System.out.println("REGISTRO INSERTADO CON HILO: "+email);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
