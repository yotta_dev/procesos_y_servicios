package a_Ejemplos_Iniciales;

import java.io.IOException;


public class RunTimeExec_Start{
	public static void main(String[] args) {
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec("gitkraken");
            
            //test line
            process.destroy();
            //process.destroy();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
	}
}
