package f_hiloC_CreacionMultiple;

public class Hilo_Calcular_Area extends Thread{
	int c;
	int area;
	int baseH;
	int alturaH;
	
	public Hilo_Calcular_Area(int c,int baseP,int alturaP) {
		
		super();
		this.c = c;
		baseH=baseP;
		alturaH=alturaP;
	}

	@Override
	public void run() {
		int area;
		area = (baseH*alturaH)/2;
		System.out.println("El Area del Triangulo "+c+" es :"+area);
		System.out.println("---------------------------------------");
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
