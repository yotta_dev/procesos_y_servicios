package f_hiloC_CreacionMultiple;

import b_ejercicio_1.Entrada;

public class Lanzadora {

	public static void main(String[] args) {
		int base,altura;
		
		for (int i = 0; i < 10; i++) {
			System.out.println("Triangulo "+i);
			System.out.print("Introduce la base: ");
			base=Entrada.entero();
			System.out.print("Introduce la altura: ");
			altura=Entrada.entero();
			Hilo_Calcular_Area calcular = new Hilo_Calcular_Area(i, base, altura);
			calcular.start();
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

	}

}
