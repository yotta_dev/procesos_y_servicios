package h_sincronizacion_Banco;

public class Banco {

	public static void main(String[] args) {
		
		Cuenta cuenta1 = new Cuenta();
		cuenta1.saldo=10011;
		
		Cliente cliente1=new Cliente(10);
		Cliente cliente2=new Cliente(10);
		
		cliente1.nombre="Fulanito";
		cliente2.nombre="Menganito";
		
		cliente1.cuenta=cuenta1;
		cliente2.cuenta=cuenta1;
		
		cliente1.start();
		cliente2.start();
		
		try {
			cliente1.join();
			cliente2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("\nCliente: "+cliente1.nombre+"\nDinero retirado: "+cliente1.total);
		System.out.println("\nCliente: "+cliente2.nombre+"\nDinero retirado: "+cliente2.total);
		System.out.println("\nSaldo: "+cuenta1.saldo);	
		

	}

}
