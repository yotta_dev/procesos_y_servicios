package h_sincronizacion_Banco;

public class Cliente extends Thread {

	Cuenta cuenta;
	String nombre;
	double cantidad;
	double total = 0;

	Cliente() {
	}

	Cliente(double cantidad) {
		this.cantidad = cantidad;
	}

	public void run() {

		/*
		 * AQU�? HE MODIFICADO EL EJERCICIO PARA EVITAR QUE FALLE,
		 * POR MUCHAS EJECUCIONES QUE HAYA HECHO NO ENCONTRÉ FALLO
		 * PERO POR LÓGICA EL PROGRAMA SIN SINCRONIZAR EL OBJETO PODR�?A
		 * FALLAR EN LA COMPROBACIÓN DE SALDO DISPONIBLE, LOS DOS HILOS
		 * PODRIAN ACCEDER A LA MISMA VARIABLE Y DAR PASO A LA RESTA,
		 * EL TEMA EST�? EN QUE NO FALLA NUNCA YA QUE POR MUCHAS VUELTAS
		 * QUE HAGA EL BUCLE LA ÚNICA OPORTUNIDAD DE FALLAR ES EN LA ÚLTIMA
		 * ÚNICAMENTE, POR ESO PARECE QUE NO FALLA, PERO PODR�?A FALLAR.
		 * 
		 * (EL FALLO SER�?A DAR SALDO NEGATIVO AL FINALIZAR
		 * 
		 * DE ESTA MANERA NO FALLAR�?A
		 */
			while (true) {
				synchronized (cuenta) { //SINCRONIZO EL OBJETO
					if (cuenta.comprobar(cantidad)) {
						cuenta.restar(cantidad);
						total += cantidad;
					} else
						break;
				}

				try {
					Thread.sleep(1); //CEDO EL TESTIGO
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		

	}

}
