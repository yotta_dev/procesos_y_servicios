package j_UD_1_3_Sockets_Ejercicio2;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class A_Servidor {

	public static void main(String[] args) {

		final int PUERTO_ECO = 5000;
		ServerSocket serverSocket = null;
		Socket socket = null;
		PrintWriter printWriter;

		try {
			
			int cnt = 0;

			serverSocket = new ServerSocket();

			InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost", PUERTO_ECO);

			serverSocket.bind(inetSocketAddress);

			while (cnt < 3) {

				socket = serverSocket.accept();
				
				System.out.println("Cliente conectado");

				cnt++;

				printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

				if (socket != null && printWriter != null) {

					// ENVIAMOS MENSAJE AL CLIENTEC
					printWriter.println("Hola Cliente" + cnt);
					
				}
				socket.close();
			}
			
			serverSocket.close();

		} catch (IOException e) {

			System.err.println("Fallo en la conexion: " + e);

		}
	}

}
