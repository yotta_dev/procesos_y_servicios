package j_UD_1_3_Sockets_Ejercicio2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class B_Cliente {

	public static void main(String[] args) {

		final int PUERTOECO = 5000;
		Socket socket_Cliente = null;
		BufferedReader bufferedReader;

		try {
			socket_Cliente = new Socket();

			InetSocketAddress address = new InetSocketAddress("localhost", PUERTOECO);

			socket_Cliente.connect(address);

			System.out.println("Ya estoy autorizado");

			bufferedReader = new BufferedReader(new InputStreamReader(socket_Cliente.getInputStream()));

			String mensaje = bufferedReader.readLine();

			System.out.println("Mensaje: " + mensaje);
			
			socket_Cliente.close();

			bufferedReader.close();

		} catch (UnknownHostException e) {
			System.err.println("Máquina desconocida: maquina");
		} catch (IOException e) {
			System.err.println("Fallo en la conexión: " + e);
		}

	}

}
