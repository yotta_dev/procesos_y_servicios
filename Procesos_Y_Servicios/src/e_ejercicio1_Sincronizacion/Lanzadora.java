package e_ejercicio1_Sincronizacion;

public class Lanzadora {
	public static int enteroCompartido=3;
	
	public static void main(String[] args) {
		
		for (int i = 0; i < 100; i++) {
			
			if (i%2==0) {
				Incrementadora aumentar = new Incrementadora(i);
				aumentar.start();
				System.out.println("state: "+enteroCompartido);
			}else {
				Decrementadora reducir = new Decrementadora(i);
				reducir.start();
				System.out.println("state: "+enteroCompartido);
			}
		}
		System.out.println("Resultado Tras Ejecucion:"+enteroCompartido);
	}

}
