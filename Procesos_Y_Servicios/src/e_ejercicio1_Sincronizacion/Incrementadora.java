package e_ejercicio1_Sincronizacion;

public class Incrementadora extends Thread {
	
	int i;
	
	public Incrementadora(int i) {
		super();
		this.i = i;
		System.out.println("Hilo Incrementador Numero:"+i);
	}

	@Override
	public void run() {
		Lanzadora.enteroCompartido++;
	}
}
