package e_ejercicio1_Sincronizacion;

public class Decrementadora extends Thread {
	int i;

	public Decrementadora(int i) {
		super();
		this.i = i;
		System.out.println("Hilo Decrementador Numero:"+i);
	}

	@Override
	public void run() {
		Lanzadora.enteroCompartido--;
	}
}
