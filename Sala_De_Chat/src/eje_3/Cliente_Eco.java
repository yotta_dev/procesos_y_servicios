package eje_3;

import java.net.*;
import java.io.*;

class ClienteEco {
	public static void main(String args[]) {
		final int PUERTOECO = 5000;
		Socket socket_Cliente = null;
		BufferedReader bufferedReader;
		PrintWriter printWriter;
		BufferedReader input_Teclado = new BufferedReader(new InputStreamReader(System.in));
		String texto;

		try {
			socket_Cliente = new Socket();

			InetSocketAddress address = new InetSocketAddress("localhost", PUERTOECO);

			socket_Cliente.connect(address);

			bufferedReader = new BufferedReader(new InputStreamReader(socket_Cliente.getInputStream()));

			printWriter = new PrintWriter(new OutputStreamWriter(socket_Cliente.getOutputStream()), true);

			if (socket_Cliente != null && bufferedReader != null && printWriter != null) {

				System.out.print("Introduce cadena: ");

				while ((texto = input_Teclado.readLine()) != null) {

					// ENVIAMOS CADENA DE TEXTO AL SERVER
					printWriter.println(texto);

					System.out.println(">>ECHO: " + bufferedReader.readLine());

					// Solicitamos cadena de nuevo
					System.out.print("Introduce cadena: ");
				}

				// RESPUESTA ECHO DEL SERVIDOR
				System.out.println("Saliendo");
				printWriter.close();
				bufferedReader.close();
				socket_Cliente.close();
				input_Teclado.close();
			}
		} catch (UnknownHostException e) {
			System.err.println("Máquina desconocida: maquina");
		} catch (IOException e) {
			System.err.println("Fallo en la conexión: " + e);
		}
	}
}
