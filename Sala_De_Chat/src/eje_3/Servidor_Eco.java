package eje_3;

import java.net.*;
import java.io.*;

class ServidorEco {
	public static void main(String args[]) {

		final int PUERTO_ECO = 5000;
		ServerSocket serverSocket = null;
		Socket socket = null;
		BufferedReader bufferedReader;
		PrintWriter printWriter;
		String texto;

		try {

			serverSocket = new ServerSocket();

			InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost", PUERTO_ECO);

			serverSocket.bind(inetSocketAddress);
						
			socket = serverSocket.accept();

			printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

			bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			if (socket != null && bufferedReader != null && printWriter != null) {

				while (true) {
					//LEEMOS MENSAJE DEL CLIENTE
					texto = bufferedReader.readLine();					
					System.out.println("Recibiendo: "+texto);
					//REENVIAMOS MENSAJE AL CLIENTEC
					printWriter.println(texto);

				}
			}

			socket.close();
			serverSocket.close();

		} catch (IOException e) {

			System.err.println("Fallo en la conexion: " + e);

		}
	}

}