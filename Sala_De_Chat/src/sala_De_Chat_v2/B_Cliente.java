package sala_De_Chat_v2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

public class B_Cliente {

	public static void main(String[] args) {

		try {
			/* SOCKET CLIENTE PARA LA CONEXION CON EL SERVIDOR Y LA SALA DE CHAT */
			System.out.println(" - Iniciando Cliente de Sala de Chat ");
			Socket socket_Cliente = new Socket();

			/* INFORMACION DE RED DEL SERVIDOR PARA LA CONEXION DEL CLIENTE */
			String server_Address = "localhost";// String modificable en el que almacenamos la informacion del servidor
			int puerto = 1313;// Integer en el que almacenamos el puerto de conexion a nuestro Servidor
			// Toda esta información de Red la alamacenamos en un objeto InetSocketAdress
			// para posteriormente chacer el Bind al Server y levantarlo para comenzar a
			// realizar escuchas esperando clientes
			InetSocketAddress inetSocketAddress = new InetSocketAddress(server_Address, puerto);

			System.out.println(" - Conectandose al Servidor ...");
			socket_Cliente.connect(inetSocketAddress);

			/* STREAMS DE ENTRADA Y SALIDA PARA LA COMUNICACION DE LOS MENSAJES */
			BufferedReader bufferedReader_Server = new BufferedReader(
					new InputStreamReader(socket_Cliente.getInputStream()));
			PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket_Cliente.getOutputStream()));

			System.out.print(" - Conectado a la Sala de Chat \n - Introduzca su Nombre de Usuario: ");
			BufferedReader input_Teclado = new BufferedReader(new InputStreamReader(System.in));
			String nombreUsuario = input_Teclado.readLine();

			printWriter.println(nombreUsuario);

			Boolean conectado = true;

			while (conectado) {
				
				/* Salida de mensajes */
				System.out.print(" - Introduzca un Mensaje: ");
				String mensaje = input_Teclado.readLine();
				if (!mensaje.equals("disconnect")) {
					
					printWriter.println(mensaje);
					printWriter.flush();
				} else {
					printWriter.println(mensaje);
					printWriter.flush();
					conectado = false;
				}
				System.out.println(bufferedReader_Server.readLine());
				
			}

			System.out.println(" - Te has Desconectado -");
			// bufferedReader_Server.close();
			printWriter.close();
			socket_Cliente.close();

		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}

	}

}
