package sala_De_Chat_v2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class C_Hilo_Mensaje extends Thread {

	BufferedReader bufferedReader_readServer;
	PrintWriter printWriter;
	String nombreUsuario;

	public C_Hilo_Mensaje(BufferedReader bufferedReader, PrintWriter printWriter, String nombreUsuario) {
		super();
		this.bufferedReader_readServer = bufferedReader;
		this.printWriter = printWriter;
		this.nombreUsuario = nombreUsuario;
	}

	@Override
	public void run() {

		try {

			while (true) {

				String mensajeRecibido = bufferedReader_readServer.readLine();

				if (!mensajeRecibido.equals("disconnect")) {
					System.out.println("- " + nombreUsuario + " dice : " + mensajeRecibido);//Pinta mensaje en el servidor
					printWriter.println(" - " + nombreUsuario + " dice : " + mensajeRecibido);//Deberia comunicar mensaje a todos 
					printWriter.flush();
				}
			}
		} catch (IOException e) {
			System.err.println("Fallo en la conexi�n: " + e);
		} catch (NullPointerException e) {
			A_Server.cnt--;
			System.out.println("- " + nombreUsuario + " se ha desconectado - Users online: " + A_Server.cnt);
			printWriter.println("- " + nombreUsuario + " se ha desconectado - Users online: " + A_Server.cnt);
			printWriter.flush();

		}
		super.run();
	}

}
