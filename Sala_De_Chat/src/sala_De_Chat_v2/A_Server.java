package sala_De_Chat_v2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class A_Server {
	public static int cnt = 0;// Contador para manejar en total de Usuarios Conectados

	public static void main(String[] args) {

		try {		

			/* SOCKETS PARA MANEJAR EL SERVIDOR Y LOS CLIENTES */
			Socket socket_Client = new Socket();// Objeto Socket que usaremos para manejar las conexion de los clientes
			ServerSocket server_Socket = new ServerSocket();// Objeto ServerSocket con el que crearemos nuestro Servidor

			/* INFORMACION DE RED DEL SERVIDOR */
			String server_Address = "localhost";// String modificable en el que almacenamos la informacion del servidor
			int puerto = 1313;// Integer en el que almacenamos el puerto de conexion a nuestro Servidor
			// Toda esta información de Red la alamacenamos en un objeto InetSocketAdress
			// para posteriormente chacer el Bind al Server y levantarlo para comenzar a
			// realizar escuchas esperando clientes
			InetSocketAddress inetSocketAddress = new InetSocketAddress(server_Address, puerto);

			/* STREAMS DE ENTRADA Y SALIDA PARA LA COMUNICACION ENTRE SERVIDOR Y CLIENTES */
			BufferedReader bufferedReader_inputClientes;
			PrintWriter printWriter_outputAClientes;

			System.out.println("------------------------------------------------------------");
			System.out.println("- Server - Sala de Chat ");
			server_Socket.bind(inetSocketAddress);

			while (true) {
				
				socket_Client = server_Socket.accept();
				System.out.println("- Cliente conectado  ");
				
				bufferedReader_inputClientes = new BufferedReader(new InputStreamReader(socket_Client.getInputStream()));
				printWriter_outputAClientes = new PrintWriter(new OutputStreamWriter(socket_Client.getOutputStream()));
				
				cnt++;
				
				String nombreUsuario = bufferedReader_inputClientes.readLine();
				System.out.println("- Bienvenido " + nombreUsuario + " - Users Online : " +cnt);
				printWriter_outputAClientes.println(" - Bienvenido " + nombreUsuario + " - Users Online : " +cnt);
				printWriter_outputAClientes.flush();
				
				C_Hilo_Mensaje c_Hilo_Mensaje = new C_Hilo_Mensaje(bufferedReader_inputClientes,printWriter_outputAClientes,nombreUsuario);
				
				c_Hilo_Mensaje.start();
				
				//REENVIAR MENSAJES DE ALGUNA MANERA
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

}
