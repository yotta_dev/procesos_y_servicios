package sala_De_Chat_v1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class B_Cliente {

	public static void main(String[] args) {

		try {
			String direccionServidor = "localhost";
			int puerto = 1313;

			Socket socket_Cliente = new Socket();
			InetSocketAddress address = new InetSocketAddress(direccionServidor, puerto);

			System.out.println("- Conectando con el Servidor -");
			socket_Cliente.connect(address);
			System.out.println("- Conectado -");
			// Lanzamos hilo y nos comunicamos en bucle hasta introducir disconnect
			C_Hilo_Mensaje c_Hilo_Mensaje = new C_Hilo_Mensaje(socket_Cliente);

			c_Hilo_Mensaje.run();

			BufferedReader bufferedReader_readServer = new BufferedReader(new InputStreamReader(socket_Cliente.getInputStream()));
			PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket_Cliente.getOutputStream()), true);
			
			String mensajeCliente;
			while ((mensajeCliente = bufferedReader_readServer.readLine()) != null) {

				printWriter.println(mensajeCliente);// Mando mensaje por tuberia de salida a todos aquellos
													// hilos que esten escuchando el Stream de salida del
													// programa
			}

			System.out.println("- Fin Conexi�n con el Servidor -");
		} catch (UnknownHostException e) {
			System.err.println("Máquina desconocida: maquina");
		} catch (IOException e) {
			System.err.println("Fallo en la conexión: " + e);
		}
	}

}
