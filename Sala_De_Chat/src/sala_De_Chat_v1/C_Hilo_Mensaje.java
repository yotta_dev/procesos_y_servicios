package sala_De_Chat_v1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class C_Hilo_Mensaje extends Thread {

	Socket socket_Cliente;

	BufferedReader bufferedReader_readServer;
	BufferedReader input_Teclado = new BufferedReader(new InputStreamReader(System.in));

	PrintWriter printWriter;

	public C_Hilo_Mensaje(Socket socket_Cliente) {
		super();
		this.socket_Cliente = socket_Cliente;
	}

	@Override
	public void run() {
		super.run();

		try {

			// Stream de Entrada del Cliente
			bufferedReader_readServer = new BufferedReader(new InputStreamReader(socket_Cliente.getInputStream()));
			// Stream de Salida del Cliente
			printWriter = new PrintWriter(new OutputStreamWriter(socket_Cliente.getOutputStream()), true);

			// Recibimos el numero de cliente asignado por el Servidor
			// y la solicitud de introducir un nombre del cliente
			String mensaje = bufferedReader_readServer.readLine();
			System.out.println("- Servidor dice: " + mensaje);

			// Introducimos el nombre del Cliente
			System.out.print("- Introduce un nombre de usuario : ");
			String nombreDeUsuario = input_Teclado.readLine();

			// Mandamos al Servidor el Nombre del Cliente
			printWriter.println(nombreDeUsuario);

			Boolean conectado = true;

			while (conectado) {

				String mensajeRecibido = bufferedReader_readServer.readLine();
				if (mensajeRecibido != null) {
					System.out.println("- Servidor:" + mensajeRecibido);
				}

				System.out.print("- Introduzca su mensaje: ");
				String mensajeAEnviar = input_Teclado.readLine();
				
				if (!mensajeAEnviar.equals("disconnect")) {
					
					printWriter.println(mensajeAEnviar);
					
				} else {
					
					printWriter.println(nombreDeUsuario + " se ha desconectado.");
					printWriter.println("null");
					A_Servidor.cnt--;
					conectado = false;
					socket_Cliente.close();
				}

			}

			bufferedReader_readServer.close();

		} catch (UnknownHostException e) {
			System.err.println("Máquina desconocida: maquina");
		} catch (IOException e) {
			System.err.println("Fallo en la conexión: " + e);
		}

	}

}
