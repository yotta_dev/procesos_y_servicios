package eje_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Crear una clase Java que sea capaz de sumar todos los n�meros comprendidos
 * entre dos valores incluyendo ambos valores. Para resolverlo crearemos una
 * clase Sumador que tenga un m�todo que acepte dos n�meros n1 y n2 y que
 * devuelva la suma de todo el intervalo. Adem�s, incluiremos un m�todo main que
 * ejecute la operaci�n de suma tomando los n�meros de la l�nea de comandos (es
 * decir, se pasan como argumentos al main). Una vez hecha la prueba de la clase
 * sumador, le quitamos el main, y crearemos una clase que sea capaz de lanzar
 * varios procesos.
	
 */
public class LanzadorSumador {

	public static void main(String[] args) throws IOException {
		Process process = new ProcessBuilder(args).start();
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		System.out.println("Salida del proceso " + Arrays.toString(args) + ":");
		System.out.println("El resultado es: " );
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		br.close();
	}

}
