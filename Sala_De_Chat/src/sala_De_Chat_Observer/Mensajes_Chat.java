package sala_De_Chat_Observer;

import java.util.Observable;

public class Mensajes_Chat extends Observable {
	
	private String mensaje;
	
	public Mensajes_Chat() {
		
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
		this.setChanged();//Indica que el mensaje se ha modificado
		//Notifica a los observadores que el mensaje ha cambiado y se lo pasa
		this.notifyObservers(this.getMensaje());
	}
	
	
}
