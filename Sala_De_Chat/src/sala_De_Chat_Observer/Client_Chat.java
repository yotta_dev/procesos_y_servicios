package sala_De_Chat_Observer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;

//CLASE DEL TIPO THREAD_CLIENT_CHAT
public class Client_Chat extends Thread implements Observer {
	final int PUERTOECO = 1313;
	Socket socket_Cliente = null;

	BufferedReader input_Teclado;
	BufferedReader bufferedReader_Mensajes;
	PrintWriter printWriter;
	Mensajes_Chat mensajes_Chat;

	public Client_Chat(Socket socket_Cliente, Mensajes_Chat mensajes) {
		try {
			this.socket_Cliente = socket_Cliente;
			this.mensajes_Chat = mensajes;
			// Input por Teclado
			input_Teclado = new BufferedReader(new InputStreamReader(System.in));
			// Stream de Entrada del Cliente
			bufferedReader_Mensajes = new BufferedReader(new InputStreamReader(socket_Cliente.getInputStream()));
			// Stream de Salida del Cliente
			printWriter = new PrintWriter(new OutputStreamWriter(socket_Cliente.getOutputStream()), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		String mensajeRecibido;
		boolean isConnected = true;

		// Añadimos a este Cliente a la Lista de Observers del Chat
		mensajes_Chat.addObserver(this);

		while (isConnected) {

			try {				
				
				socket_Cliente = new Socket();

				InetSocketAddress address = new InetSocketAddress("localhost", PUERTOECO);

				System.out.println("- Conectando con el Servidor -");
				socket_Cliente.connect(address);
				System.out.println("- Conectado -");

				// Recibimos el numero de cliente asignado por el Servidor
				// y la solicitud de introducir un nombre del cliente
				String mensaje = bufferedReader_Mensajes.readLine();
				System.out.println("- Servidor: " + mensaje);

				// Introducimos el nombre del Cliente
				System.out.print("- Introduce un nombre de usuario : ");
				String nombreDeUsuario = input_Teclado.readLine();

				// Mandamos al Servidor el Nombre del Cliente
				printWriter.println(nombreDeUsuario);
				
				// Leemos mensaje enviado por el cliente
				mensajeRecibido = bufferedReader_Mensajes.readLine();
				// De esta forma ponemos a disposicion de todos los observadores la cadena de texo introducida
				mensajes_Chat.setMensaje(mensajeRecibido);

				socket_Cliente.close();
				System.out.println("- Fin Conexión con el Servidor -");
				bufferedReader_Mensajes.close();

			} catch (UnknownHostException e) {
				System.err.println("Máquina desconocida: maquina");
			} catch (IOException e) {
				System.err.println("Fallo en la conexión: " + e);
			}
		}

	}

	@Override
	public void update(Observable o, Object arg) {
		try {
			//Enviamos mensaje al cliente
			printWriter.write(arg.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

/*
 * // Recibimos la Contraseña generada por el Servidor String contraseña =
 * bufferedReader_readServer.readLine();
 * System.out.println("- Servidor - Su Contraseña es: " + contraseña);
 */
