package sala_De_Chat;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class B_Cliente {

	public static void main(String[] args) {

		final int PUERTOECO = 1313;
		Socket socket_Cliente = null;

		try {
			socket_Cliente = new Socket();

			InetSocketAddress address = new InetSocketAddress("localhost", PUERTOECO);

			System.out.println("- Conectando con el Servidor -");
			socket_Cliente.connect(address);
			System.out.println("- Conectado -");
			
			//Lanzamos hilo y nos comunicamos en bucle hasta introducir disconnect
			C_Hilo_Mensaje c_Hilo_Mensaje = new C_Hilo_Mensaje(socket_Cliente);
			
			c_Hilo_Mensaje.run();
			
			socket_Cliente.close();
			System.out.println("- Fin Conexión con el Servidor -");

		} catch (UnknownHostException e) {
			System.err.println("Máquina desconocida: maquina");
		} catch (IOException e) {
			System.err.println("Fallo en la conexión: " + e);
		}

	}

}
