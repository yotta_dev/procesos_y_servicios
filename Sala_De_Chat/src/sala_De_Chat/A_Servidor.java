package sala_De_Chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class A_Servidor {

	public static int cnt = 0;

	public static void main(String[] args) {
		// Informacion de Red y Conexion al Server
		final int PUERTO_ECO = 1313;
		ServerSocket server_Socket = null;
		Socket socket_Cliente = null;
		// Streams Estandar de Comunicación
		PrintWriter printWriter;
		BufferedReader bufferedReader;

		try {

			server_Socket = new ServerSocket();

			InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost", PUERTO_ECO);

			server_Socket.bind(inetSocketAddress);

			while (true) {

				System.out.println("- Esperando Conexi�n Cliente -");
				socket_Cliente = server_Socket.accept();

				cnt++;// Contabilizamos el cliente conectado

				// Stream de salida del Servidor
				printWriter = new PrintWriter(new OutputStreamWriter(socket_Cliente.getOutputStream()), true);

				// Stream de Entrada del Servidor
				bufferedReader = new BufferedReader(new InputStreamReader(socket_Cliente.getInputStream()));

				// PASO 1:El servidor envía un mensaje indicando el nº de clie
				if (socket_Cliente != null && printWriter != null) {
					System.out.println("------------------------------------------------------------");
					System.out.println("- Cliente Conectado n�: " + cnt);

					// PASO 2:El servidor pide un nombre de usuario, que el usuario envía
					printWriter.println("- Bienvenido �Cual es tu nombre de Usuario?");
					String nombreCliente = bufferedReader.readLine();

					printWriter.println(" Se ha conectado " + nombreCliente + " hay " + cnt + " Usuarios conectados");

					String mensajeCliente;
					while ((mensajeCliente = bufferedReader.readLine()) != null) {

						printWriter.println(mensajeCliente);//Mando mensaje por tuberia de salida a todos aquellos hilos que esten escuchando el Stream de salida del programa

					}

					printWriter.println(nombreCliente+" se ha desconectado. Hay "+cnt+" usuarios conectados");					

				}
				socket_Cliente.close();
				
				server_Socket.close();
			}

			

		} catch (IOException e) {

			System.err.println("Fallo en la conexion: " + e);

		}
	}

}
